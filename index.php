<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/htm; charset=UTF-8">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale-1">
	<title>Bizconsultnik</title>
	<!-- Bootstrape css -->
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<!-- custom css -->
	<link rel="stylesheet" href="assets/css/custom.css">
	<!-- fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,700,700i" rel="stylesheet">
</head>
<body>
	<header>
		<div class="container">
			<ul>
				<li><a href="#about">About</a></li>
				<li><a href="#contact-us">Contact us</a></li>
			</ul>
			<div class="logo">
				<img src="assets/images/logo.PNG" alt="Logo-image">
			</div>
			<h1>Engineering your business plans</h1>
			<p>Consulting company</p>
		</div>
	</header>

	<main>
		<section id="about">
			<div class="title-block">
				<p>What we offer</p>
				<hr>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-lg-4 col-lg-offset-1">
						<div class="content m-15">
							<p>Business planning</p>
							<img src="assets/images/content-img-1.png" alt="Image">
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 col-lg-offset-2">
						<div class="content m-15">
							<p>Organisational optimisation</p>
							<img src="assets/images/content-img-2.png" alt="Image">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-lg-4 col-lg-offset-1">
						<div class="content m-15">
							<p>Operational research</p>
							<img src="assets/images/content-img-3.png" alt="Image">
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 col-lg-offset-2">
						<div class="content m-15">
							<p>Start-up guidance</p>
							<img src="assets/images/content-img-4.png" alt="Image">
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="container-fluid decoration">
			<div class="col-sm-6 custom"></div>
			<div class="col-sm-6 custom"></div>
		</div>
		<section id="contact-us">
			<div class="container">
				<form>
					<div class="title-block">
						<p>Contact us</p>
						<hr>
					</div>
					<input type="text" name = "name" placeholder="Name:">
					<input type="text" name = "subject" placeholder="Subject:">
					<textarea placeholder="Enquiry:" cols="30" rows="5" name = "enquiry"></textarea>
					<div class="message alert"></div>
					<div class="g-recaptcha" data-callback="recaptchaCallback" data-sitekey="6Le0GxAUAAAAALQ4prptg8GGACsiKdz5vkzcPcr7"></div>
          <input class="button" value="Submit" id="mail">
				</form>
			</div>
		</section>
		<section>
			<div class="container-fluid map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d16080.20238358653!2d34.77908594401744!3d32.084226060309334!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sca!2ses!4v1482671584956" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</section>
	</main>
	<footer>Created by: <a href="http://tqg.com.ua/">tqg.com.ua</a></footer>
	<script src="assets/js/jquery-2.1.1.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/custom.js"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
</body>
</html>