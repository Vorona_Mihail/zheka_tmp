$(document).ready(function(){
	captcha = false;
	message = $(".message");
	message.hide();
	$("#mail").click(function(){
		send_email();
	});
	//scroll-top function
	$('a[href^="#"]').click(function(){
		var target = $(this).attr('href');
		var y = $(target).offset().top - 54;
		$('html, body').animate({scrollTop: y}, 1000);
		return false;
	});
});

function recaptchaCallback() {
  captcha = true;
}

function send_email(){
	var name = $('input[name="name"]').val();
	var subject = $('input[name="subject"]').val();
	var enquiry = $('textarea[name="enquiry"]').val();
	if (name&&subject&&enquiry&&captcha){
		$.ajax({
		  type: "POST",
		  url: "post.php",
		  data: "name="+name+"&subject="+subject+"&enquiry="+enquiry,
		  success: function(){
		  	message.show();
		    message.html('<strong>Success!</strong> '+name+', we will contact soon.');
		    message.addClass('alert-success');
		    message.removeClass('alert-danger');
		    $('input[name="name"]').val('');
		    $('input[name="subject"]').val('');
		    $('textarea[name="enquiry"]').val('');
		  },
		  error: function(){
		  	message.show();
		    message.html('<strong>Fail!</strong> Something went wrong');
		    message.addClass('alert-danger');
		    message.removeClass('alert-success');
		  },
		});
	}
	else {
		message.show();
		message.html('<strong>Error!</strong> Fill in all the fields and confirm that you are not a robot, please.');
		message.addClass('alert-danger');
		message.removeClass('alert-success');
	}
}
